function ApplicationCtrl($scope, $http, $timeout, $store) {
  var userApiURL = 'https://kanbanery.com/api/v1/user.json';

  var _u = function(url, apiToken) {
    return url + '?callback=JSON_CALLBACK&api_token=' + apiToken;
  }

  $scope.user = $store.get('user') || null;

  $scope.clearAlert = function() { $scope.alert = null };

  $scope.showAlert = function(type, message) {
    $scope.alert = { type: type, message: message };
    $timeout($scope.clearAlert, 5000);
  }

  $scope.login = function() {
    var apiToken = prompt('Your Kanbanery API token');

    $http({ method: 'JSONP', url: _u(userApiURL, apiToken) }).
      success(function(data, status) {
        $scope.user = data;
        $store.set('user', data);
        $scope.showAlert('success', 'Logged in as ' + data.name);
      }).
      error(function(data, status) {
        $scope.showAlert('error', 'Shit');
      });
  }

  $scope.logout = function() {
    $scope.user = null;
    $store.reset();
    $scope.showAlert('success', 'Logged out.');
  }
}
