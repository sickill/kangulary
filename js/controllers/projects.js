function ProjectListCtrl($scope, $http, $store) {
  var workspacesApiURL = 'https://kanbanery.com/api/v1/user/workspaces.json';
  var columnsApiURL = 'https://WORKSPACE.kanbanery.com/api/v1/projects/PROJECT_ID/columns.json'
  var tasksApiURL = 'https://WORKSPACE.kanbanery.com/api/v1/projects/PROJECT_ID/tasks.json'

  var _u = function(url) {
    return url + '?callback=JSON_CALLBACK&api_token=' + $scope.user.api_token;
  }

  var loadWorkspaces = function() {
    $http({ method: 'JSONP', url: _u(workspacesApiURL) }).
      success(function(data, status) {
        $scope.workspaces = data;
        $store.set('workspaces', data);
      }).
      error(function(data, status) {
        $scope.error = data;
    });
  }

  var loadColumns = function(project) {
    var url = columnsApiURL.replace('WORKSPACE', project.workspace.name).replace('PROJECT_ID', project.id);

    $http({ method: 'JSONP', url: _u(url) }).
      success(function(data, status) {
        project.columns = data;
        // $scope.projectLoading = false;
      });
  }

  var loadTasks = function(project) {
    var url = tasksApiURL.replace('WORKSPACE', project.workspace.name).replace('PROJECT_ID', project.id);

    $http({ method: 'JSONP', url: _u(url) }).
      success(function(data, status) {
        project.tasks = data;
        // $scope.projectLoading = false;
      });
  }

  $scope.project = null;

  if ($scope.user) {
    $scope.workspaces = $store.get('workspaces') || [];
    loadWorkspaces();
  } else {
    $scope.workspaces = [];
  }

  $scope.$watch('user', function(newUser, oldUser) {
    if (newUser != oldUser) {
      $scope.workspaces = [];
      $scope.project = null;

      if (newUser) {
        loadWorkspaces();
      }
    }
  });

  $scope.setProject = function(project, workspace) {
    project.workspace = workspace;
    $scope.project = project;
    loadColumns(project);
    loadTasks(project);
    // $scope.projectLoading = true;
  };

  $scope.columnTasks = function(project, column) {
    var tasks = [];

    (project.tasks || []).forEach(function(task) {
      if (task.column_id == column.id) {
        tasks.push(task);
      }
    });

    return tasks;
  }
}
