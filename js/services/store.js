app.service('$store', function() {
  var getData = function() {
    var json = localStorage['kangulary'];
    return (json ? JSON.parse(json) : {});
  }

  var setData = function(data) {
    localStorage['kangulary'] = JSON.stringify(data);
  }

  this.set = function(key, value) {
    var data = getData();
    data[key] = value;
    setData(data);
  }

  this.get = function(key, defaultValue) {
    return getData()[key];
  }

  this.reset = function() {
    setData({});
  }
})
